Source: kompare
Section: kde
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Norbert Preining <norbert@preining.info>,
           Sune Vuorela <sune@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-sequence-kf6,
               dh-sequence-pkgkde-symbolshelper,
               cmake (>= 3.16~),
               extra-cmake-modules (>= 6.0.0~),
               gettext,
               libkf6codecs-dev (>= 6.0.0~),
               libkf6config-dev (>= 6.0.0~),
               libkf6coreaddons-dev (>= 6.0.0~),
               libkf6doctools-dev (>= 6.0.0~),
               libkf6i18n-dev (>= 6.0.0~),
               libkf6iconthemes-dev (>= 6.0.0~),
               libkf6jobwidgets-dev (>= 6.0.0~),
               libkf6parts-dev (>= 6.0.0~),
               libkf6texteditor-dev (>= 6.0.0~),
               libkf6widgetsaddons-dev (>= 6.0.0~),
               libkomparediff2-dev (>= 4:24.08~),
               qt6-base-dev (>= 6.5.0~),
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://apps.kde.org/kompare
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/kompare
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/kompare.git

Package: kompare
Architecture: any
Section: devel
Depends: kpart6-kompare (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Suggests: khelpcenter,
Description: file difference viewer
 Kompare displays the differences between files.  It can compare the
 contents of files or directories, as well as create, display, and merge patch
 files.
 .
 This package is part of the KDE Software Development Kit module.

Package: kpart6-kompare
Architecture: any
Section: devel
Depends: ${misc:Depends}, ${shlibs:Depends},
Breaks: kpart5-kompare,
Replaces: kpart5-kompare,
Description: file difference viewer - kpart interface component
 Kompare displays the differences between files.  It can compare the
 contents of files or directories, as well as create, display, and merge patch
 files.
 .
 This package contains the kpart needed by applications to embed kompare in
 them.
 .
 This package is part of the KDE Software Development Kit module.

Package: libkompareinterface-dev
Architecture: any
Section: libdevel
Depends: libkompareinterface6 (= ${binary:Version}), ${misc:Depends},
Description: file difference viewer - kpart interface library development files
 Kompare displays the differences between files.  It can compare the
 contents of files or directories, as well as create, display, and merge patch
 files.
 .
 This package contains the development files for the kpart needed by
 applications to embed kompare in them.
 .
 This package is part of the KDE Software Development Kit module.

Package: libkompareinterface6
Architecture: any
Multi-Arch: same
Section: libs
Depends: ${misc:Depends}, ${shlibs:Depends},
Recommends: kpart6-kompare,
Description: file difference viewer - kpart interface library
 Kompare displays the differences between files.  It can compare the
 contents of files or directories, as well as create, display, and merge patch
 files.
 .
 This package contains the shared library for the kpart interface component.
 .
 This package is part of the KDE Software Development Kit module.
